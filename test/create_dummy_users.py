"""Module to import some dummy data to test the application"""

from typing import Optional
import random

from bugtracker import database
from bugtracker.definitions import UserCreate, BugCreate


if __name__ == "__main__":
    database.init()

    users_list = [
        UserCreate("Elias Bouchard", "elias.bouchard@magnusarchives.london.org"),
        UserCreate("Basira Hussain", "basira.hussain@magnusarchives.london.org"),
        UserCreate("Michael Shelley", "michael.shelley@magnusarchives.london.org"),
    ]
    users_dict: dict[int, UserCreate] = {}
    for user in users_list:
        user_id = database.add_user(user)
        users_dict[user_id] = user

    potential_users: list[Optional[int]] = list(users_dict.keys())
    potential_users.append(None)
    bug_list = [
        BugCreate(
            "Worms everywhere",
            assigned_user=None,
            description="Why are there worms everywhere can we get rid of them? "
            "Slapping them doesn't seem to work",
        ),
        BugCreate(
            "Being watched",
            assigned_user=None,
            description="Many visitors report feeling like they're being watched, "
            "please can we do something about this?",
        ),
        BugCreate(
            "Unknown table",
            assigned_user=None,
            description="This table arrived and I just can't stop looking at it..."
            "I've covered it for now but what if someone gets stuck looking at it",
        ),
    ]
    for bug in bug_list:
        i = random.randint(0, len(potential_users) - 1)
        bug.assigned_user = potential_users[i]
        bug_id = database.add_bug(bug)

else:
    raise Exception("Shouldn't be called as a module")
