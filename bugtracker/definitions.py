from dataclasses import dataclass
from datetime import datetime
from typing import Optional


@dataclass
class UserCreate:
    name: Optional[str]
    email: Optional[str]


@dataclass
class User:
    id: int
    name: str
    email: str


@dataclass
class BugCreate:
    title: Optional[str] = None
    assigned_user: Optional[int] = None
    description: Optional[str] = None


@dataclass
class Bug:
    id: int
    created: datetime
    title: str
    assigned_user: Optional[int] = None
    description: Optional[str] = None
