import pathlib
import logging

PROJECT_PATH = pathlib.Path(__file__).parent.resolve()
TEMPLATES_PATH = PROJECT_PATH / "templates"

LOGGER = logging.getLogger(__name__)
