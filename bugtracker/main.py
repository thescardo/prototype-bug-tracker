import logging

from flask import Flask, render_template, redirect, url_for, request, flash

from bugtracker import utils
from bugtracker import database
from bugtracker import definitions

database.init()
app = Flask("BugTracker", template_folder=str(utils.TEMPLATES_PATH.absolute()))

LOGGER = logging.getLogger(__name__)


@app.route("/")
@app.route("/index")
def index():
    return render_template("index.html")


@app.route("/bug/<int:bug_id>")
def bug(bug_id: int):
    bug = database.get_bug(bug_id)
    if bug.assigned_user is None:
        assigned_user_name = "Not assigned"
    else:
        assigned_user_name = f"Assigned to {database.get_user(bug.assigned_user).name}"

    if bug.description == "" or bug.description is None:
        bug.description = "No description added yet"
    return render_template("bug.html", bug=bug, assigned_user_name=assigned_user_name)


@app.route("/bugs")
def all_bugs():
    bugs = database.get_all_bugs()
    return render_template("bugs_list.html", bugs=bugs)


@app.route("/raise_bug", methods=("GET", "POST"))
def raise_bug():
    if request.method == "POST":
        title = request.form["title"]
        description = request.form["description"]
        assigned_user = request.form.get("assigned_user")

        if "None" in assigned_user:
            try:
                assigned_user = int(assigned_user)
            except ValueError:
                assigned_user = None

        if not title:
            flash("Title is required!")
        elif not description:
            flash("Description is required!")
        else:
            bug_id = database.add_bug(
                definitions.BugCreate(
                    title=title, description=description, assigned_user=assigned_user
                )
            )
            return redirect(url_for("bug", bug_id=bug_id))

    users = database.get_all_users()
    return render_template("raise_bug.html", users=users)


@app.route("/modify_bug/<int:bug_id>", methods=("GET", "POST"))
def modify_bug(bug_id: int):
    if request.method == "POST":
        title = request.form.get("title")
        description = request.form.get("description")
        assigned_user_val = request.form.get("assigned_user")

        if assigned_user_val is not None and "None" not in assigned_user_val:
            try:
                assigned_user = int(assigned_user_val)
            except ValueError:
                assigned_user = None
        else:
            assigned_user = None

        bug = database.modify_bug(
            bug_id,
            definitions.BugCreate(
                title=title if title != "" else None,
                description=description if description != "" else None,
                assigned_user=assigned_user,
            ),
        )

        return redirect(url_for("bug", bug_id=bug.id))

    bug = database.get_bug(bug_id)
    if bug.assigned_user is None:
        assigned_user_name = "Not assigned"
    else:
        assigned_user_name = f"Assigned to {database.get_user(bug.assigned_user).name}"
    users = database.get_all_users()
    return render_template(
        "modify_bug.html", bug=bug, assigned_user_name=assigned_user_name, users=users
    )


@app.route("/user/<int:user_id>")
def user(user_id):
    user = database.get_user(user_id)
    return render_template("user.html", user=user)


@app.route("/modify_user/<int:user_id>", methods=("GET", "POST"))
def modify_user(user_id: int):
    if request.method == "POST":
        name = request.form.get("name")
        email = request.form.get("email")

        user = database.modify_user(
            user_id,
            definitions.UserCreate(
                name=name if name != "" else None, email=email if email != "" else None
            ),
        )

        return redirect(url_for("user", user_id=user.id))

    user = database.get_user(user_id)
    return render_template("modify_user.html", user=user)


@app.route("/users")
def all_users():
    users = database.get_all_users()
    return render_template("users_list.html", users=users)


@app.route("/create_user", methods=("GET", "POST"))
def create_user():
    if request.method == "POST":
        name = request.form["name"]
        email = request.form["email"]

        if not name:
            flash("Name is required!")
        elif not email:
            flash("Email is required!")
        else:
            user_id = database.add_user(definitions.UserCreate(name=name, email=email))
            return redirect(url_for("user", user_id=user_id))

    return render_template("create_user.html")


if __name__ == "__main__":
    app.run(debug=True)
