import sqlite3
from contextlib import contextmanager
import dataclasses

from bugtracker import utils
from bugtracker import definitions
from werkzeug.exceptions import abort

DATABASE_FOLDER = utils.PROJECT_PATH / "database"

DATABASE_NAME = "bugtracker.db"
SCHEMA_FILENAME = "schema.sqlite"

DATABASE_PATH = DATABASE_FOLDER / DATABASE_NAME
SCHEMA_PATH = DATABASE_FOLDER / SCHEMA_FILENAME


def init():
    connection = sqlite3.connect(str(DATABASE_PATH))

    with open(str(SCHEMA_PATH)) as f:
        connection.executescript(f.read())

    connection.close()


@contextmanager
def get_connection():
    try:
        conn = sqlite3.connect(str(DATABASE_PATH))
        conn.row_factory = sqlite3.Row

        yield conn
    finally:
        conn.close()


def add_user(user: definitions.UserCreate) -> int:
    with get_connection() as conn:
        cursor = conn.cursor()
        cursor.execute(
            "INSERT INTO users (name, email) VALUES (?, ?)",
            (user.name, user.email),
        )
        user_id = cursor.lastrowid
        conn.commit()

        return user_id


def modify_user(user_id: int, changes: definitions.UserCreate) -> definitions.User:
    with get_connection() as conn:
        changed_dict = {
            k: v for k, v in dataclasses.asdict(changes).items() if v is not None
        }

        params_list: list = list(changed_dict.values())
        params_list.append(user_id)
        params_tuple = tuple(params_list)
        placeholder_str = ",".join([f"{x}=?" for x in changed_dict.keys()])

        conn.execute(f"UPDATE users SET {placeholder_str} WHERE id=?", params_tuple)
        conn.commit()

        return get_user(user_id)


def get_user(user_id: int) -> definitions.User:
    with get_connection() as conn:
        user = conn.execute("SELECT * FROM users WHERE id = ?", (user_id,)).fetchone()

        if user is None:
            abort(404)

        return definitions.User(**user)


def get_all_users() -> list[definitions.User]:
    with get_connection() as conn:
        result = conn.execute("SELECT * FROM users")

        if result is None:
            abort(404)

        users = []
        for row in result:
            users.append(definitions.User(**row))
        return users


def add_bug(bug: definitions.BugCreate) -> int:
    with get_connection() as conn:
        cursor = conn.cursor()
        cursor.execute(
            "INSERT INTO bugs (assigned_user, " "title, description) VALUES (?, ?, ?)",
            (bug.assigned_user, bug.title, bug.description),
        )
        bug_id = cursor.lastrowid
        conn.commit()
        return bug_id


def modify_bug(bug_id: int, changes: definitions.BugCreate) -> definitions.Bug:
    with get_connection() as conn:
        # Assigned user is an option, so always change it
        changed_dict = {
            k: v
            for k, v in dataclasses.asdict(changes).items()
            if v is not None or k == "assigned_user"
        }

        params_list: list = list(changed_dict.values())
        params_list.append(bug_id)
        params_tuple = tuple(params_list)
        placeholder_str = ",".join([f"{x}=?" for x in changed_dict.keys()])

        conn.execute(f"UPDATE bugs SET {placeholder_str} WHERE id=?", params_tuple)
        conn.commit()

        return get_bug(bug_id)


def get_bug(bug_id: int) -> definitions.Bug:
    with get_connection() as conn:
        bug = conn.execute("SELECT * FROM bugs WHERE id = ?", (bug_id,)).fetchone()

        if bug is None:
            abort(404)

        return definitions.Bug(**bug)


def get_all_bugs() -> list[definitions.Bug]:
    with get_connection() as conn:
        result = conn.execute("SELECT * FROM bugs")

        if result is None:
            abort(404)

        bugs = []
        for row in result:
            bugs.append(definitions.Bug(**row))
        return bugs
