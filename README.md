# prototype-bug-tracker

Simple bug tracker web interface.

# Dependencies

All Python dependencies are described in requirements.txt

Only sqlite3 is required on the server machine (tested against Ubuntu 22.10).

# Create database

sqlite3 bugtracker.db '.read database/schema.sqlite'

# Testing

No unit tests, but you can populate the application with some dummy data using the script in the test folder:
`PYTHONPATH=. python test/create_dummy_users.py`

Run the flask app using `flask --app bugtracker/main --debug run`